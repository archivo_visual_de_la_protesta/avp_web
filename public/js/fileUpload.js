$(function() {
    // Configure Cloudinary
    // with the credentials on
    // your Cloudinary dashboard
    $.cloudinary.config({ cloud_name: 'dh7lz7t14', api_key: '553315941772278'});
    // Upload button
    var uploadButton = $('#submit');

    // Upload-button event
    uploadButton.on('click', function(e){
        var timestamp = Date.now()
        var infoLabel = $('#author').val() + '_|_' + $('#date').val() + '_|_' + $('#place').val()+ '_|_' + timestamp;
        var encodedString = btoa(infoLabel);
        // alert(encodedString);
        // Initiate upload
        cloudinary.openUploadWidget({ cloud_name: 'dh7lz7t14', upload_preset: 'p6x9zzxy', tags: ['cgal'], public_id: encodedString},
        function(error, result) {
            if(error) console.log(error);
            // If NO error, log image data to console
            var id = result[0].public_id;
            // console.log(processImage(id));
        });
    });
})
function processImage(id) {
    var options = {
        client_hints: true,
    };
    var url = $.cloudinary.url('cgal', {format: 'json', type: 'list'});
    // console.log('url')
    console.log(id)
    imgUrl = 'https://res.cloudinary.com/dh7lz7t14/image/upload/' + id;
    $('#gallery').append('<img src="' + imgUrl + '" />')
    return '<img src="https://'+ $.cloudinary.url(id, options) +'" style="width: 100%; height: auto"/>';
}
